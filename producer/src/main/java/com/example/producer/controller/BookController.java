package com.example.producer.controller;

import com.example.producer.dto.request.BookRequest;
import com.example.producer.entity.Book;
import com.example.producer.service.book.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/book")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @GetMapping
    public String sendBook(@RequestBody BookRequest bookRequest) {
        return bookService.sendBook(bookRequest);
    }
}
