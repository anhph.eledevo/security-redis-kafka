package com.example.producer.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Book {
    private Long id;
    private String name;
    private String author;
}
