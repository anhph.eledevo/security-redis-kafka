package com.example.producer.service.book;

import com.example.producer.dto.request.BookRequest;
import org.springframework.stereotype.Service;

@Service
public interface BookService {
    String sendBook(BookRequest book);
}
