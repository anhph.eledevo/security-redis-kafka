package com.example.producer.service.book;

import com.example.producer.dto.request.BookRequest;
import com.example.producer.entity.Book;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class BookServiceImpl implements BookService{
    @Value("${application.kafka.hoanganh-topic}")
    private String topic;

    private final KafkaTemplate<String, BookRequest> kafkaTemplate;
    @Override
    public String sendBook(BookRequest book) {
        kafkaTemplate.send(topic, book);
        return "Gửi thông tin sách đi rồi nhé!!!";
    }
}
