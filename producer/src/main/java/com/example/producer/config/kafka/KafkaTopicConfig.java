package com.example.producer.config.kafka;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;

public class KafkaTopicConfig {
    @Value("${application.kafka.hoanganh-topic}")
    private String topic;

    @Bean
    public NewTopic eledevoTopic() {
        return TopicBuilder
                .name(topic)
                .build();
    }
}
