package com.example.consumerone.dto.request;

import lombok.*;

@Getter
@NoArgsConstructor
public class BookRequest {
    private String name;
    private String author;
}

