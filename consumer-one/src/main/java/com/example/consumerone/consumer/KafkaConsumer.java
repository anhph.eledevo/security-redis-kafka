package com.example.consumerone.consumer;

import com.example.consumerone.dto.request.BookRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Service
@Slf4j
public class KafkaConsumer {
    @KafkaListener(topics = "HoangAnh", groupId = "hoanhGroup")
    public void consumeMsg(String message) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        BookRequest bookRequest = objectMapper.readValue(message, BookRequest.class);
        System.out.println(bookRequest.getName());
        System.out.println(bookRequest.getAuthor());
    }
}
